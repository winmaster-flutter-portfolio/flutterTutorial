import '../logic/ingredients.dart';

class Selector
{
  //private properties
  List<Ingredient> _ingredients;
  int _currentIngredientIndex = -1;
  int _amount = 0;

  Selector(this._ingredients)
  {
    _ingredients.shuffle();
  }

//getters

  List<Ingredient> get ingredients => _ingredients;
  int get length => _ingredients.length;
  int get ingredientNumber => _currentIngredientIndex + 1;
  int get amount => _amount;

  Ingredient get nextIngredient
  {
    _currentIngredientIndex++;
    if (_currentIngredientIndex >= length) return null;
    return _ingredients[_currentIngredientIndex];
  }

  void chosenIngredient(bool choice)
  {
    if(choice) _amount++;
  }
}