import 'package:flutter/material.dart';
import '../widgets/like_button.dart';
import '../widgets/ingredient_text.dart';
import '../widgets/my_dialog.dart';
import '../logic/ingredients.dart';
import '../logic/selector.dart';
import '../views/ending.dart';

class IngredientView extends StatefulWidget
{
  @override
  State createState() => new IngredientViewState();
}

class IngredientViewState extends State<IngredientView>
{
  Ingredient currentIngredient;
   Selector selector = new Selector([
    new Ingredient("garlic"),
    new Ingredient("jalapeno"),
    new Ingredient("chorizzo"),
    new Ingredient("pineapple"),
    new Ingredient("banana")
  ]);


  String ingredientText;
  int ingredientNumber;
  bool isChosen;
  bool showDialog = false;

  @override
  void initState()
  {
    super.initState();
    currentIngredient = selector.nextIngredient;
    ingredientText = currentIngredient.ingredientName;
    ingredientNumber = selector.ingredientNumber;
  }

  void handleUserChoice(bool choice)
  {
    isChosen = choice;
    selector.chosenIngredient(isChosen);
    this.setState(()
    {
      showDialog = true;
    });
  }

  void handleUserDialogClick()
  {
    if(selector.length == ingredientNumber)
    {
      Navigator.of(context).pushAndRemoveUntil(new MaterialPageRoute(builder: (BuildContext context) => new EndPage()), (Route route) => route == null);
    }

    currentIngredient = selector.nextIngredient;
    this.setState(()
    {
      showDialog = false;
      ingredientText = currentIngredient.ingredientName;
      ingredientNumber = selector.ingredientNumber;
    });
  }


  @override
  Widget build(BuildContext context)
  {
    return new Stack
      (
        children:<Widget>[
        new Column(
            children: <Widget>[
              new LikeButton(true, () => handleUserChoice(true)),
              new IngredientText(ingredientText),
              new LikeButton(false, () => handleUserChoice(false)),
            ]
        ),
         showDialog == true ? new MyDialog(isChosen, handleUserDialogClick) : new Container()
      ]
    );
  }
}