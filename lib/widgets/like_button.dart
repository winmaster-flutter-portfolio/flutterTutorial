import 'package:flutter/material.dart';

class LikeButton extends StatelessWidget
{
  //true - like, false - dislike

  final bool _answer;
  final VoidCallback _onTap;

  LikeButton(this._answer, this._onTap);

  @override
  Widget build(BuildContext context)
  {
    return new Expanded(
      child: new Material(
        color: _answer == true ? Colors.lightBlue : Colors.pinkAccent,
        child: new InkWell(
          onTap: () => _onTap(),
          child: new Center(
              child: new Container(
                decoration: new BoxDecoration(
                    border: new Border.all(color: Colors.white, width: 5.0)
                ),
                padding: new EdgeInsets.all(20.0),
                child: new Text(_answer == true ? "Like" : "Dislike",
                    style: new TextStyle(color: Colors.white, fontSize: 40.0,
                        fontWeight: FontWeight.bold, fontStyle: FontStyle.italic)
                ),
              )
          ),
        ),
      ),
    );
  }
}